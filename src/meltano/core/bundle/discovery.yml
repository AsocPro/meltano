version: 3.0
extractors:
  - name: tap-google-analytics
    namespace: google-analytics
    pip_url: "git+https://gitlab.com/meltano/tap-google-analytics.git"
    settings:
      - name: key_file_location
        env: GOOGLE_ANALYTICS_API_CLIENT_SECRETS
        kind: file
      - name: view_id
        label: View ID
      - name: reports
      - name: start_date
        kind: date_iso8601
      - name: end_date
        kind: date_iso8601
  - name: tap-gitlab
    namespace: gitlab
    pip_url: "git+https://gitlab.com/meltano/tap-gitlab.git"
    settings:
      - name: api_url
        label: GitLab API URL
        value: "https://gitlab.com/api/v4"
      - name: private_token
        env: GITLAB_API_PRIVATE_TOKEN
      - name: groups
        value: ""
        env: GITLAB_API_GROUPS
      - name: projects
        value: ""
        env: GITLAB_API_PROJECTS
      - name: ultimate_license
        value: false
        kind: boolean
        env: GITLAB_API_ULTIMATE_LICENSE
      - name: start_date
        kind: date_iso8601
        env: GITLAB_API_START_DATE
    docs: https://meltano.com/docs/plugins.html#gitlab
  - name: tap-zendesk
    namespace: zendesk
    pip_url: "tap-zendesk"
    settings:
      - name: email
        kind: email
      - name: api_token
        label: Zendesk API Token
      - name: subdomain
        label: Zendesk Domain
        description: <subdomain>.zendesk.com
      - name: start_date
        kind: date_iso8601
  - name: tap-zuora
    namespace: zuora
    pip_url: "tap-zuora"
    settings:
      - name: username
      - name: password
        kind: password
      - name: partner_id
      - name: api_token
      - name: api_type
      - name: sandbox
        kind: boolean
      - name: start_date
        kind: date_iso8601
    docs: https://meltano.com/docs/plugins.html#tap-zuora
  - name: tap-marketo
    namespace: marketo
    pip_url: "git+https://gitlab.com/meltano/tap-marketo.git"
    settings:
      - name: identity
      - name: client_id
        label: Client ID
      - name: client_secret
      - name: endpoint
      - name: start_time
        kind: date_iso8601
  - name: tap-salesforce
    namespace: salesforce
    pip_url: "git+https://gitlab.com/meltano/tap-salesforce.git"
    settings:
      # TODO: remove, should be injected from Meltano
      - name: client_id
        label: Client ID
      - name: username
      - name: password
      - name: security_token
      - name: api_type
        value: REST
      - name: select_fields_by_default
        value: true
        kind: boolean
      - name: start_date
        kind: date_iso8601
  - name: tap-mongodb
    namespace: mongodb
    pip_url: "git+https://github.com/singer-io/tap-mongodb.git"
    settings:
      - name: user
      - name: password
        kind: password
      - name: host
      - name: port
        value: 27017
      - name: dbname
        label: Database Name
  - name: tap-stripe
    namespace: stripe
    pip_url: "git+https://github.com/meltano/tap-stripe.git@v0.2.4"
    settings:
      - name: account_id
        label: Account ID
      - name: client_secret
      - name: start_date
        kind: date_iso8601
  - name: tap-fastly
    namespace: fastly
    pip_url: "git+https://gitlab.com/meltano/tap-fastly.git"
    settings:
      - name: api_token
        label: API Token
      - name: start_date
        label: Start Date
        kind: date_iso8601
  - name: tap-carbon-intensity
    namespace: carbon
    pip_url: "git+https://gitlab.com/meltano/tap-carbon-intensity"
  - name: tap-csv
    namespace: tap-csv
    pip_url: "git+https://gitlab.com/meltano/tap-csv.git"
    settings:
      - name: csv_files_definition
        label: CSV Files Definition
        kind: file
        documentation: https://gitlab.com/meltano/tap-csv#run
    docs: https://meltano.com/docs/plugins.html#tap-csv
loaders:
  - name: target-csv
    namespace: csv
    pip_url: "git+https://gitlab.com/meltano/target-csv.git"
    settings:
      - name: delimiter
        value: "\t"
        description: A one-character string used to separate fields. It defaults to ','
      - name: quotechar
        value: "'"
        description: A one-character string used to quote fields containing special characters, such as the delimiter or quotechar, or which contain new-line characters. It defaults to '"'.
  - name: target-snowflake
    namespace: snowflake
    pip_url: "git+https://gitlab.com/meltano/target-snowflake.git"
    # TODO: in reality this should be a Meltano database concept
    # instead of a granular connection. We could use an existing
    # connection from analyze.
    settings:
      - name: account
      - name: username
      - name: password
        kind: password
      - name: role
      - name: database
      - name: schema
      - name: warehouse
  - name: target-postgres
    namespace: pg
    pip_url: "git+https://github.com/meltano/target-postgres.git"
    settings:
      - name: user
        env: PG_USERNAME
      - name: password
        kind: password
      - name: host
        env: PG_ADDRESS
      - name: port
      - name: dbname
        label: Database Name
        env: PG_DATABASE
      - name: schema
  - name: target-sqlite
    namespace: sqlite
    pip_url: "git+https://gitlab.com/meltano/target-sqlite.git"
    settings:
      - name: database
        description: E.g. `meltano` will create `meltano.db`
transformers:
  - name: dbt
    namespace: dbt
    pip_url: dbt
transforms:
  - name: tap-carbon-intensity
    namespace: carbon
    pip_url: "https://gitlab.com/meltano/dbt-tap-carbon-intensity.git"
    vars:
      entry_table: "{{ env_var('PG_SCHEMA') }}.entry"
      generationmix_table: "{{ env_var('PG_SCHEMA') }}.generationmix"
      region_table: "{{ env_var('PG_SCHEMA') }}.region"
  - name: tap-salesforce
    namespace: salesforce
    pip_url: "https://gitlab.com/meltano/dbt-tap-salesforce.git"
    vars:
      schema: "{{ env_var('PG_SCHEMA') }}"
  - name: tap-gitlab
    namespace: gitlab
    pip_url: "https://gitlab.com/meltano/dbt-tap-gitlab.git"
    vars:
      schema: "{{ env_var('PG_SCHEMA') }}"
      ultimate_license: "{{ env_var('GITLAB_API_ULTIMATE_LICENSE') }}"
  - name: tap-stripe
    namespace: stripe
    pip_url: "https://gitlab.com/meltano/dbt-tap-stripe.git"
    vars:
      livemode: false
      schema: "{{ env_var('PG_SCHEMA') }}"
  - name: tap-zuora
    namespace: zuora
    pip_url: "https://gitlab.com/meltano/dbt-tap-zuora.git"
    vars:
      schema: "{{ env_var('PG_SCHEMA') }}"
  - name: tap-zendesk
    namespace: zendesk
    pip_url: "https://gitlab.com/meltano/dbt-tap-zendesk.git"
    vars:
      schema: "{{ env_var('PG_SCHEMA') }}"
  - name: tap-google-analytics
    namespace: google-analytics
    pip_url: "https://gitlab.com/meltano/dbt-tap-google-analytics.git"
    vars:
      schema: "{{ env_var('PG_SCHEMA') }}"
models:
  - name: model-carbon-intensity-sqlite
    namespace: carbon
    pip_url: "git+https://gitlab.com/meltano/model-carbon-intensity-sqlite.git"
  - name: model-carbon-intensity
    namespace: carbon
    pip_url: "git+https://gitlab.com/meltano/model-carbon-intensity.git"
  - name: model-salesforce
    namespace: salesforce
    pip_url: "git+https://gitlab.com/meltano/model-salesforce.git"
  - name: model-gitflix
    namespace: gitflix
    pip_url: "git+https://gitlab.com/jschatz1/model-gitflix.git"
  - name: model-zendesk
    namespace: zendesk
    pip_url: "git+https://gitlab.com/meltano/model-zendesk.git"
  - name: model-gitlab
    namespace: gitlab
    pip_url: "git+https://gitlab.com/meltano/model-gitlab.git"
  - name: model-gitlab-ultimate
    namespace: gitlab
    pip_url: "git+https://gitlab.com/meltano/model-gitlab-ultimate.git"
  - name: model-stripe
    namespace: stripe
    pip_url: "git+https://gitlab.com/meltano/model-stripe.git"
  - name: model-zuora
    namespace: zuora
    pip_url: "git+https://gitlab.com/meltano/model-zuora.git"
  - name: model-google-analytics
    namespace: google-analytics
    pip_url: "git+https://gitlab.com/meltano/model-google-analytics.git"
orchestrators:
  - name: airflow
    namespace: airflow
    pip_url: "apache-airflow==1.10.2"
    settings:
      - name: core.dags_folder
        value: $MELTANO_PROJECT_ROOT/orchestrate/dags
        env: AIRFLOW__CORE__DAGS_FOLDER
      - name: core.plugins_folder
        value: $MELTANO_PROJECT_ROOT/orchestrate/plugins
        env: AIRFLOW__CORE__DAGS_FOLDER
      - name: core.sql_alchemy_conn
        value: sqlite:///.meltano/orchestrators/airflow/airflow.db
        env: AIRFLOW__CORE__SQL_ALCHEMY_CONN
      - name: core.load_examples
        value: False
        env: AIRFLOW__CORE__LOAD_EXAMPLES
      - name: webserver.web_server_port
        value: 5010
        env: AIRFLOW__WEBSERVER__WEB_SERVER_PORT
connections:
  - name: sqlite
    namespace: sqlite
    settings:
      - name: dbname
        label: Database
        description: Path to the database, relative to the project root.
        value: meltano.db
  - name: postgresql
    namespace: postgresql
    settings:
      - name: user
        value: warehouse
      - name: password
        value: warehouse
      - name: host
        value: 'localhost:5502'
      - name: dbname
        value: warehouse
      - name: schema
        value: analytics
        env: PG_SCHEMA
